const express = require('express');
const router = express.Router();

const { getAll,
        getOne,
        createOne,
        editOne,
        getAllProps,
        getAllPropsTypes
} = require('../controllers/persons');

// app.get('/api/persons/all', (req, res) => {
//     res.json([1, 3, 5]);
// })


router.get('/persons/all', getAll);
router.get('/person/:_id', getOne);
router.post('/person/', createOne);
router.put('/person/:_id', editOne);

router.get('/persons/props', getAllProps);
router.get('/persons/propstypes', getAllPropsTypes);

module.exports = router;