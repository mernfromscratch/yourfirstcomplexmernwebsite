const constants =  require('../constants');
const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
    Name: {
        type: String,
        unique: true
    },
    OrganizationalNumber: {
        type: Number,
        unique: true
    },
    Sector: {
        type: String, 
        enum: constants.companySectors
    },
    NetWorth: Number
});

module.exports = mongoose.model("Company", companySchema);