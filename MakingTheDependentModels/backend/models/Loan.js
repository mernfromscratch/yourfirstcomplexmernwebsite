const constants =  require('../constants');
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema;

const loanSchema = new mongoose.Schema({
    Loaner: {
        type: ObjectId,
        ref: 'Person'
    },
    AmountBorrowed: Number,
    RentPct: Number,
    AmountLeftToPayBack: Number
});

module.exports = mongoose.model("Loan", loanSchema);