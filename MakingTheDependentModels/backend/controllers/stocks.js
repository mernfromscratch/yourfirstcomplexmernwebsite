const Stock = require('../models/Stocks');
const Person = require('../models/Person');
const Company = require('../models/Company');
const { stockProps, stockPropsTypes } = require('../constants');

// exports.getAllStocks = (req, res) => {
//     res.json([1, 3, 5]);
// };

//  /api/stocks/all
exports.getAll = async (req, res) => {
    const allStocks = await Stock.find({})
        .populate('Owner')
        .populate('Company')
        .sort([['Name', 'desc']])
        .exec();
    res.json(allStocks);
}

//  /api/stock/1
exports.getOne = async (req, res) => {
    try {
        const stock = await Stock.findById(req.params._id).populate('Owner').populate('Company').exec();
        res.json(stock);            
    } catch (error) {
        res.status(400).send(`No stock with id ${req.params._id} was found`);
    }
}

//  /api/stock/
exports.createOne = async (req, res) => {
    try {
        const newStock = await new Stock({...req.body}).save();
        res.json(newStock);
    } catch (error) {
        res.status(400).send('Could not create new stock');
    }
}

//  /api/stock/1
exports.editOne = async (req, res) => {
    try {
        const editedStock = Stock.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedStock);
    } catch (error) {
        res.status(400).send(`Error updating stock with id ${req.params._id}`);
    }
}

//  /api/stock/1
exports.deleteOne = async (req, res) => {
    try {
        const deletedStock = Stock.findOneAndDelete({_id: req.params._id}).exec();
        res.json(deletedStock);
    } catch (error) {
        res.status(400).send(`Could not delete stock with id ${req.params._id}`).exec();
    }
}

//  /api/stocks/props
exports.getAllProps = async (req, res) => {
    res.json(stockProps);
}

//  /api/stocks/propsTypes
exports.getAllPropsTypes = async (req, res) => {
    const allPeople = await Person.find({})
        .sort([['Name', 'desc']])
        .exec();
    const allCompanies = await Company.find({})
        .sort([['Name', 'desc']])
        .exec();
    let stockPropsTypesOpts = stockPropsTypes.map((propType) => {
        return (
            propType.property === "Owner" ? {...propType, options: allPeople} : ( propType.property === "Company" ? {...propType, options: allCompanies} : propType )
        )
    })
    res.json(stockPropsTypesOpts);
}
