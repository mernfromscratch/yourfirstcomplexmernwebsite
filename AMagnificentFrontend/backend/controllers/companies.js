const Company = require('../models/Company');
const { companySectors, companyProps, companyPropsTypes } = require('../constants');

// exports.getAllCompanies = (req, res) => {
//     res.json([1, 3, 5]);
// };

//  /api/companies/all
exports.getAll = async (req, res) => {
    const allCompanies = await Company.find({})
        .sort([['Name', 'desc']])
        .exec();
    res.json(allCompanies);
}

//  /api/company/1
exports.getOne = async (req, res) => {
    try {
        const company = await Company.findById(req.params._id).exec();
        res.json(company);            
    } catch (error) {
        res.status(400).send(`No company with id ${req.params._id} was found`);
    }
}

//  /api/company/
exports.createOne = async (req, res) => {
    try {
        const newCompany = await new Company({...req.body, OrganizationalNumber: Math.floor(Math.random() * 10000000)}).save();
        res.json(newCompany);
    } catch (error) {
        res.status(400).send('Could not create new company');
    }
}

//  /api/company/1
exports.editOne = async (req, res) => {
    try {
        const editedCompany = Company.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedCompany);
    } catch (error) {
        res.status(400).send(`Error updating company with id ${req.params._id}`);
    }
}

//  /api/companies/sectors
exports.getAllSectors = async (req, res) => {
    res.json(companySectors);
}

//  /api/companies/props
exports.getAllProps = async (req, res) => {
    res.json(companyProps);
}

//  /api/companies/propstypes
exports.getAllPropsTypes = async (req, res) => {
    res.json(companyPropsTypes);
}