const Loan = require('../models/Loan');
const Person = require('../models/Person');
const { loanProps, loanPropsTypes } = require('../constants');

// exports.getAllLoans = (req, res) => {
//     res.json([1, 3, 5]);
// };

//  /api/loans/all
exports.getAll = async (req, res) => {
    const allLoans = await Loan.find({})
        .populate('Loaner')
        .sort([['Name', 'desc']])
        .exec();
    res.json(allLoans);
}

//  /api/loan/1
exports.getOne = async (req, res) => {
    try {
        const loan = await Loan.findById(req.params._id).populate('Loaner').exec();
        res.json(loan);            
    } catch (error) {
        res.status(400).send(`No loan with id ${req.params._id} was found`);
    }
}

//  /api/loan/
exports.createOne = async (req, res) => {
    try {
        const newLoan = await new Loan({...req.body}).save();
        res.json(newLoan);
    } catch (error) {
        res.status(400).send('Could not create new loan');
    }
}

//  /api/loan/1
exports.editOne = async (req, res) => {
    try {
        const editedLoan = Loan.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedLoan);
    } catch (error) {
        res.status(400).send(`Error updating loan with id ${req.params._id}`);
    }
}

//  /api/loan/1
exports.deleteOne = async (req, res) => {
    try {
        const deletedLoan = Loan.findOneAndDelete({_id: req.params._id}).exec();
        res.json(deletedLoan);
    } catch (error) {
        res.status(400).send(`Could not delete loan with id ${req.params._id}`).exec();
    }
}

//  /api/loans/props
exports.getAllProps = async (req, res) => {
    res.json(loanProps);
}

//  /api/loans/propsTypes
exports.getAllPropsTypes = async (req, res) => {
    const allPeople = await Person.find({})
        .sort([['Name', 'desc']])
        .exec();
    let loanPropsTypesOpts = loanPropsTypes.map((propType) => {
        return (
            propType.type === "select" ? {...propType, options: allPeople} : propType
            // Get all people
        )
    })
    res.json(loanPropsTypesOpts);
}