const BankAccount = require('../models/BankAccount');
const Person = require('../models/Person');
const { bankAccountUses, bankAccountProps, bankAccountPropsTypes } = require('../constants');

// exports.getAllBankAccounts = (req, res) => {
//     res.json([1, 3, 5]);
// };

//  /api/bankAccounts/all
exports.getAll = async (req, res) => {
    const allBankAccounts = await BankAccount.find({})
        .populate('Owner')
        .sort([['Name', 'desc']])
        .exec();
    res.json(allBankAccounts);
}

//  /api/bankAccount/1
exports.getOne = async (req, res) => {
    try {
        const bankAccount = await BankAccount.findById(req.params._id).populate('Owner').exec();
        res.json(bankAccount);            
    } catch (error) {
        res.status(400).send(`No bankAccount with id ${req.params._id} was found`);
    }
}

//  /api/bankAccount/
exports.createOne = async (req, res) => {
    try {
        const newBankAccount = await new BankAccount({...req.body}).save();
        res.json(newBankAccount);
    } catch (error) {
        res.status(400).send('Could not create new bankAccount');
    }
}

//  /api/bankAccount/1
exports.editOne = async (req, res) => {
    try {
        const editedBankAccount = BankAccount.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedBankAccount);
    } catch (error) {
        res.status(400).send(`Error updating bankAccount with id ${req.params._id}`);
    }
}

//  /api/bankAccount/1
exports.deleteOne = async (req, res) => {
    try {
        const deletedBankAccount = BankAccount.findOneAndDelete({_id: req.params._id}).exec();
        res.json(deletedBankAccount);
    } catch (error) {
        res.status(400).send(`Could not delete bankAccount with id ${req.params._id}`).exec();
    }
}

//  /api/bankAccounts/props
exports.getAllProps = async (req, res) => {
    res.json(bankAccountProps);
}

//  /api/bankAccounts/propsTypes
exports.getAllPropsTypes = async (req, res) => {
    const allPeople = await Person.find({})
        .sort([['Name', 'desc']])
        .exec();
    let bankAccountPropsTypesOpts = bankAccountPropsTypes.map((propType) => {
        return (
            propType.property === "Owner" ? {...propType, options: allPeople} : (propType.property === "AccountUse" ? {...propType, options: bankAccountUses} : propType)
            // Get all people
        )
    })
    res.json(bankAccountPropsTypesOpts);
}