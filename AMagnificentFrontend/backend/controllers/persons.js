const { personProps, personPropsTypes } = require('../constants');
const Person = require('../models/Person');

// exports.getAllPersons = (req, res) => {
//     res.json([1, 3, 5]);
// };

//  /api/persons/all
exports.getAll = async (req, res) => {
    const allPersons = await Person.find({})
        .sort([['Name', 'desc']])
        .exec();
    res.json(allPersons);
}

//  /api/person/1
exports.getOne = async (req, res) => {
    try {
        const person = await Person.findById(req.params._id).exec();
        res.json(person);            
    } catch (error) {
        res.status(400).send(`No person with id ${req.params._id} was found`);
    }
}

//  /api/person/
exports.createOne = async (req, res) => {
    try {
        const newPerson = await new Person({...req.body, SocialSecurityNumber: Math.floor(Math.random() * 1000000000)}).save();
        res.json(newPerson);
    } catch (error) {
        res.status(400).send('Could not create new person');
    }
}

//  /api/person/1
exports.editOne = async (req, res) => {
    try {
        const editedPerson = Person.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedPerson);
    } catch (error) {
        res.status(400).send(`Error updating person with id ${req.params._id}`);
    }
}

//  /api/persons/props
exports.getAllProps = async (req, res) => {
    res.json(personProps);
}

//  /api/persons/propstypes
exports.getAllPropsTypes = async (req, res) => {
    res.json(personPropsTypes);
}
