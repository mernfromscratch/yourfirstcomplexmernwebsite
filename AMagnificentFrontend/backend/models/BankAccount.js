const constants =  require('../constants');
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema;

const bankAccountSchema = new mongoose.Schema({
    Owner: {
        type: ObjectId,
        ref: 'Person'
    },
    MoneyInAccount: Number,
    AccountUse: {
        type: String,
        enum: constants.bankAccountUses
    }
});

module.exports = mongoose.model("BankAccount", bankAccountSchema);