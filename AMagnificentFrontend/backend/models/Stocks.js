const constants =  require('../constants');
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema;

const stocksSchema = new mongoose.Schema({
    Owner: {
        type: ObjectId,
        ref: 'Person'
    },
    Company: {
        type: ObjectId,
        ref: 'Company'
    },
    Amount: {
        type: Number,
        min: 0
    }
});

module.exports = mongoose.model("Stocks", stocksSchema);