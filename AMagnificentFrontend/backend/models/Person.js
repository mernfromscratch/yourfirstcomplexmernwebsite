const constants =  require('../constants');
const mongoose = require('mongoose');

const personSchema = new mongoose.Schema({
    Name: String,
    Age: {
        type: Number,
        min: 0,
    },
    SocialSecurityNumber: {
        type: Number,
        unique: true,
    }
});

module.exports = mongoose.model("Person", personSchema);