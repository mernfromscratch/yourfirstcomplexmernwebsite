const express = require('express');
const router = express.Router();

const { getAll,
        getOne,
        createOne,
        editOne,
        getAllSectors,
        getAllProps,
        getAllPropsTypes
} = require('../controllers/companies');

// app.get('/api/companies/all', (req, res) => {
//     res.json([1, 3, 5]);
// })


router.get('/companies/all', getAll);
router.get('/company/:_id', getOne);
router.post('/company/', createOne);
router.put('/company/:_id', editOne);

router.get('/companies/sectors', getAllSectors);
router.get('/companies/props', getAllProps);
router.get('/companies/propstypes', getAllPropsTypes);


module.exports = router;