const express = require('express');
const router = express.Router();

const { getAll,
        getOne,
        createOne,
        editOne,
        deleteOne,
        getAllProps,
        getAllPropsTypes
} = require('../controllers/bankAccounts');

// app.get('/api/bankAccounts/all', (req, res) => {
//     res.json([1, 3, 5]);
// })


router.get('/bankAccounts/all', getAll);
router.get('/bankAccount/:_id', getOne);
router.post('/bankAccount/', createOne);
router.put('/bankAccount/:_id', editOne);
router.delete('/bankAccount/:_id', deleteOne);

router.get('/bankAccounts/props', getAllProps);
router.get('/bankAccounts/propsTypes', getAllPropsTypes);


module.exports = router;