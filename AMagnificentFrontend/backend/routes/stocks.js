const express = require('express');
const router = express.Router();

const { getAll,
        getOne,
        createOne,
        editOne,
        deleteOne,
        getAllProps,
        getAllPropsTypes
} = require('../controllers/stocks');

// app.get('/api/stocks/all', (req, res) => {
//     res.json([1, 3, 5]);
// })


router.get('/stocks/all', getAll);
router.get('/stock/:_id', getOne);
router.post('/stock/', createOne);
router.put('/stock/:_id', editOne);
router.delete('/stock/:_id', deleteOne);

router.get('/stocks/props', getAllProps);
router.get('/stocks/propsTypes', getAllPropsTypes);

module.exports = router;