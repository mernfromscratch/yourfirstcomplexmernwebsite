const express = require('express');
const router = express.Router();

const { getAll,
        getOne,
        createOne,
        editOne,
        deleteOne,
        getAllProps,
        getAllPropsTypes,
} = require('../controllers/loans');

// app.get('/api/loans/all', (req, res) => {
//     res.json([1, 3, 5]);
// })


router.get('/loans/all', getAll);
router.get('/loan/:_id', getOne);
router.post('/loan/', createOne);
router.put('/loan/:_id', editOne);
router.delete('/loan/:_id', deleteOne);

router.get('/loans/props', getAllProps);
router.get('/loans/propsTypes', getAllPropsTypes);


module.exports = router;