exports.personProps = ['Name', 'Age', 'SocialSecurityNumber'];
exports.personPropsTypes = [{property: 'Name', type: 'text'}, {property: 'Age', type: 'number', min: 0}];

exports.companySectors = ["Agriculture", "Health", "Transport", "Security", "Energy", "Financials", "Industry", "Information Technology"];
exports.companyProps = ['Name', 'Sector', 'NetWorth', 'OrganizationalNumber'];
exports.companyPropsTypes = [{property: 'Name', type: 'text'}, {property: 'Sector', type: 'select', options: this.companySectors}, {property: 'NetWorth', type: 'number'}];

exports.bankAccountUses = ['Savings', 'Usable', 'Pension'];
exports.bankAccountProps = ['Owner', 'MoneyInAccount'];
exports.bankAccountPropsTypes = [{property: 'Owner', type: 'select'}, {property: 'MoneyInAccount', type: 'number'}, {property: 'AccountUse', type: 'select'}];

exports.loanProps = ['Loaner', 'AmountBorrowed', 'RentPct', 'AmountLeftToPayBack'];
exports.loanPropsTypes = [{property: 'Loaner', type: 'select'}, {property: 'AmountBorrowed', type: 'number'}, {property: 'RentPct', type: 'number'}, {property: 'AmountLeftToPayBack', type: 'number'}];

exports.stockProps = ['Owner', 'Company', 'Amount'];
exports.stockPropsTypes = [{property: 'Owner', type: 'select'}, {property: 'Company', type: 'select'}, {property: 'Amount', type: 'number'}];
