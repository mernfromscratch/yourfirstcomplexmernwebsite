import {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {modelCRUD} from './fetchModels';
import {urls, fetchConstPropsTypesFromBack} from './fetchConstPropsAndTypes';
import Form from './Form';

const Edit = ({history}) => {

    const [modelEditing, setModelEditing] = useState({});
    const [propsTypesOptsModel, setPropsTypesOptsModel] = useState([]);
    const [loading, setLoading] = useState(true);

    const {type, id} = useParams();

    const sendEditToBack = async () => {
        await modelCRUD(type, 'PUT', modelEditing);
        history.push('');
    };

    const cancel = () => {
        history.push('/');
    }

    const getModelAndItsProps = async () => {
        const model = await modelCRUD(type, 'GET', {_id: id});
        const typeLowerCase = type[0].toLowerCase() + type.slice(1);
        const propsTypesOpts = await fetchConstPropsTypesFromBack(urls[typeLowerCase + 'PropsTypes']);
        console.log(model, propsTypesOpts);
        setModelEditing(model);
        setPropsTypesOptsModel(propsTypesOpts);
        setLoading(false);
    };

    useEffect(() => {
        getModelAndItsProps();
    }, []);

    return (
        <>
            {loading 
                ?   <div>Loading</div>
                :   <Form send={sendEditToBack} cancel={cancel} model={modelEditing} setModel={setModelEditing} propsTypesOptsModel={propsTypesOptsModel} editOrCreate={'edit'} />
            }
        </>
    );
}

export default Edit;