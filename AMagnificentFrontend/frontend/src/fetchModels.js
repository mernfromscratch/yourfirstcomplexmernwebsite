const {modelNamesExchange} = require("./constants");

const methodsWithId = ["PUT", "GET", "DELETE"];

exports.modelCRUD = async (modelName, method, model) => {
    try {
            let url = process.env.REACT_APP_BACKENDURL + modelNamesExchange[modelName] + "/";     // http://localhost:8000/api/person/
            if (methodsWithId.includes(method)) {
                url = url + model._id;                              // http://localhost:8000/api/person/1
            }
            const response = method === 'GET' 
                ?   await fetch(url, {
                        method: method,
                        headers: {
                            'Content-Type': 'application/json'
                    }})
                :   await fetch(url, {
                        method: method,
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(model)
                    });
            const returnedModel = await response.json();
            return returnedModel;
    } catch (error) {
        return error;
    };
};

exports.modelGetAll = async (modelNameAll) => {
    try {
            let url = process.env.REACT_APP_BACKENDURL + modelNameAll + "/all";     // http://localhost:8000/api/persons/all
            const response = await fetch(url, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            const models = await response.json();
            return models;
    } catch (error) {
        return error;
    };
};