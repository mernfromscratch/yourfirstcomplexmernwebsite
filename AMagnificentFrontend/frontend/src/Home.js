import {useState, useEffect} from 'react';
import List from './List';
import {urls, fetchConstPropsTypesFromBack} from './fetchConstPropsAndTypes';
import {modelCRUD, modelGetAll} from './fetchModels';
import {modelNamesPlural} from './constants';

const Home = () => {

    // const [state, setState] = useState({});
    const [modelsToDisplay, setModelsToDisplay] = useState([]);
    const [modelName, setModelName] = useState("");
    const [propsTypesModel, setPropsTypesModel] = useState([]);
    const [hidden, setHidden] = useState(true);

    useEffect(() => {}, [propsTypesModel, modelsToDisplay]);

    const getAllOfModelAndItsProps = async (name) => {
        setHidden(true);

        const models = await modelGetAll(name);
        const nameFirstLetterLowercase = name[0].toLowerCase() + name.slice(1);
        const urlPropsTypes = urls[nameFirstLetterLowercase + "PropsTypes"];
        const propsTypes = await fetchConstPropsTypesFromBack(urlPropsTypes);

        setModelName(name);
        setPropsTypesModel(propsTypes);
        setModelsToDisplay(models);
        setHidden(false);
    };

    const deleteItemOfModel = async (model) => {
        await modelCRUD(modelName, 'DELETE', model);
        getAllOfModelAndItsProps(modelName);
    };

    const showAllButtons = () => {
        let buttons = [];
        for (const name of modelNamesPlural) (
            buttons.push(
                <button key={name} onClick={() => getAllOfModelAndItsProps(name)} className={'model-button'}>
                    Show {name}
                </button>)
        )
        return buttons;
    }

    return (
        <div>
            <div>
                {showAllButtons()}
            </div>
            { hidden 
                ?   <div></div> 
                :   <div>
                        <List modelList={modelsToDisplay} propsTypes={propsTypesModel} modelName={modelName} updateModel={getAllOfModelAndItsProps} deleteItem={deleteItemOfModel}/>
                    </div>
            }
        </div>
    );
}

export default Home;