import {useState, useEffect} from 'react';
import Form from './Form';
import {modelCRUD} from './fetchModels';
import {modelNamesPlural} from './constants';
import {urls, fetchConstPropsTypesFromBack} from './fetchConstPropsAndTypes';

const Create = ({history}) => {

    const [modelCreating, setModelCreating] = useState({});
    const [modelName, setModelName] = useState("");
    const [propsTypesOptsModel, setPropsTypesOptsModel] = useState([]);
    const [hidden, setHidden] = useState(true);

    useEffect(() => {}, [modelCreating, propsTypesOptsModel]);

    const buttonSetPropsTypesName = async (url, mName) => {
        setHidden(false);
        setModelName(mName);
        const propsTypes = await fetchConstPropsTypesFromBack(url);
        setPropsTypesOptsModel(propsTypes);
    }

    const makeButtons = () => {
        const modelNamesLow = modelNamesPlural.map(name => ( {nameUppercase: name, nameLowercase: name.charAt(0).toLowerCase() + name.slice(1)} ));     // BankAccounts => {nameUppercase: BankAccounts, nameLowercase: bankAccounts}
        const modelNamesPropsTypes = modelNamesLow.map((namesUpperAndLower) => ({...namesUpperAndLower, propsTypesUrl: urls[namesUpperAndLower.nameLowercase + "PropsTypes"]}));

        return (
            modelNamesPropsTypes.map(namesPropsTypes => (
                <button key={namesPropsTypes.nameLowercase} onClick={() => buttonSetPropsTypesName(namesPropsTypes.propsTypesUrl, namesPropsTypes.nameUppercase)}>
                    {namesPropsTypes.nameUppercase}
                </button>
            ))
        )
    };

    const create = async () => {
        try {
            modelCRUD(modelName, "POST", modelCreating);
            history.push('/');
        } catch (error) {
            alert(error);
        }
    };

    const cancel = () => {
        setHidden(true);
    };

    return (
        <div>
            <div>
                {makeButtons()}
            </div>
            <div style={{ visibility:(hidden ? "hidden" : "visible") }}>
                <Form send={create} cancel={cancel} model={modelCreating} setModel={setModelCreating} propsTypesOptsModel={propsTypesOptsModel} editOrCreate={'create'} />
            </div>
        </div>
    );
}

export default Create;