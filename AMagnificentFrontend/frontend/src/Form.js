import {useEffect} from 'react';

const Form = ({send, cancel, model, setModel, propsTypesOptsModel, editOrCreate}) => {

    const setDefaultValuesModel = () => {
        let defaultValuesModel = {};
        const defaultValuesTypes = {text: "", number: 0};
        for (const propTypeOpt of propsTypesOptsModel) {
            defaultValuesModel[propTypeOpt.property] = propTypeOpt.type === "select" ? propTypeOpt.options[0] : defaultValuesTypes[propTypeOpt.type];
        }
        setModel(defaultValuesModel);
    };

    useEffect(() => {
        if (editOrCreate === 'create') {
            setDefaultValuesModel();   
        }
    }, [propsTypesOptsModel])

    const makeInputField = (inputDescription) => {
        const type = inputDescription.type;
        const property = inputDescription.property;
        const options = inputDescription.options;

        return (
            <tr key={property}>
                <td style={{textAlign: 'left'}}>
                    {property}
                </td>
                <td>
                    {type === "select" ? 
                        <select
                            onChange={(event) => setModel({...model, [property]: options[0]._id ? JSON.parse(event.target.value) : event.target.value})}
                            value={options[0]._id ? JSON.stringify(model[property]) : model[property]}
                        >
                            {options.map(option => (
                                <option key={option._id ? option._id : option} value={option._id ? JSON.stringify(option) : option}>
                                    {option.Name ? option.Name : option}
                                </option>
                            ))}
                        </select>
                     : 
                        <input 
                            type={type}
                            onChange={(event) => setModel({...model, [property]: event.target.value})}
                            min={propsTypesOptsModel.min}
                            max={propsTypesOptsModel.max}
                            value={model[property]}
                        />
                    }
                </td>
            </tr>
        )
    };

    const sendToBack = (event) => {
        event.preventDefault();
        send();
    };

    const cancelEditOrCreate = (event) => {
        event.preventDefault();
        cancel();
    }

    return (
        <form>
            <table>
                <tbody>
                    {propsTypesOptsModel !== [] 
                        ?   propsTypesOptsModel.map((description) => (
                                makeInputField(description)
                            ))
                        : ""
                    }
                </tbody>
            </table>
            <div>
                <button onClick={(event) => sendToBack(event)}>
                    Submit
                </button>
                <button onClick={(event) => cancelEditOrCreate(event)}>
                    Cancel
                </button>
            </div>
        </form>
    )
};

export default Form;