import {Switch, Route, Link} from 'react-router-dom';
import Home from './Home';
import Edit from './Edit';
import Create from './Create';

import './App.css';

function App() {

  return (
    <div className="App">
      <div className={'top-bar'}>
        <Link to={'/'} >
          Home
        </Link>
        <Link to={'/create/'}>
          Create
        </Link>
      </div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/edit/:type/:id/" component={Edit} />
        <Route path="/create/" component={Create} />
      </Switch>
    </div>
  );
}

export default App;
