exports.urls = {
    personsProps: process.env.REACT_APP_BACKENDURL + "persons/props",
    personsPropsTypes: process.env.REACT_APP_BACKENDURL + "persons/propsTypes",

    companiesProps: process.env.REACT_APP_BACKENDURL + "companies/props",
    companiesPropsTypes: process.env.REACT_APP_BACKENDURL + "companies/propsTypes",
    companiesSectors: process.env.REACT_APP_BACKENDURL + "companies/sectors",

    bankAccountsProps: process.env.REACT_APP_BACKENDURL + "bankAccounts/props",
    bankAccountsPropsTypes: process.env.REACT_APP_BACKENDURL + "bankAccounts/propsTypes",

    loansProps: process.env.REACT_APP_BACKENDURL + "loans/props",
    loansPropsTypes: process.env.REACT_APP_BACKENDURL + "loans/propsTypes",

    stocksProps: process.env.REACT_APP_BACKENDURL + "stocks/props",
    stocksPropsTypes: process.env.REACT_APP_BACKENDURL + "stocks/propsTypes",
};

// ("personsProps") => {return ["Name", "Age"]}
// ("personsPropsTypes") => {return [{property: 'Name', type: 'text'}, {property: 'Age', type: 'number', min: 0}]}
// ("companiesSectors") => {return ["Agriculture", "Health", "Transport", "Security", "Energy", "Financials", "Industry", "Information Technology"]}
exports.fetchConstPropsTypesFromBack = async (relevantUrl) => {
    try {
        const response = await fetch(relevantUrl);
        const constPropsTypes = await response.json();
        return constPropsTypes;
    } catch (error) {
        return error;
    };
};