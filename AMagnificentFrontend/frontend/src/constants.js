exports.modelNamesPlural = [
    "Persons", 
    "Companies", 
    "BankAccounts", 
    "Loans", 
    "Stocks"
];

exports.modelNamesExchange = {
    "Person": "Persons",
    "Company": "Companies",
    "BankAccount": "BankAccounts",
    "Loan": "Loans",
    "Stock": "Stocks",
    "Persons": "Person",
    "Companies": "Company",
    "BankAccounts": "BankAccount",
    "Loans": "Loan",
    "Stocks": "Stock"
};

exports.modelNamesSingular = [
    "Person", 
    "Company", 
    "BankAccount", 
    "Loan", 
    "Stock"
];