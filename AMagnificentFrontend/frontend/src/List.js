import {useHistory} from 'react-router-dom';

const List = ({modelList, propsTypes, modelName, deleteItem}) => {
    
    const history = useHistory();

    const modelRow = (model) => (
        <tr key={model._id}>
            {propsTypes && propsTypes.map(propType => (
                <td key={propType.property}>
                    {model[propType.property]._id ? model[propType.property].Name : model[propType.property]}
                </td>
            ))}
            <td>
                <button onClick={() => history.push('/edit/' + modelName + '/' + model._id)}>
                    Edit
                </button>
                <button onClick={() => deleteItem(model)}>
                    Delete
                </button>
            </td>
        </tr>
    );

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        {propsTypes && propsTypes.map(propType => (
                            <th key={propType.property}>
                                {propType.property}
                            </th>
                        ))}
                        <th>
                            Actions
                        </th>                        
                    </tr>
                </thead>
                <tbody>
                {modelList && modelList.map(model => (
                    modelRow(model)
                ))}
                </tbody>
            </table>
        </div>
    );
};

export default List;